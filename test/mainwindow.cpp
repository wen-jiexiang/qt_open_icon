﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "buttontest.h"
#include "dialogtest.h"
#include "animationtest.h"
#include "wheeltest.h"
#include "toast.h"
#include "timetotal.h"
#include "desktop.h"
#include "listtest.h"
#include "digitalinputtest.h"
#include "progressbar2.h"
#include "navbartest.h"
#include "labeltest.h"
#include "titlebar.h"
#include "mylistwidget.h"
#include "databasewig.h"
#include "tools/serialdebug/serialdebug.h"
#include "tools/encrypt/aes/qaesencryption.h"
#include "utils/progressbar/testprogress.h"
#include "utils/label/marqueelabeltest.h"
#include "utils/button/mybuttontest.h"
#include "utils/progressbar/myprogresstest.h"
#include "utils/input/lineeditwidgettest.h"
#include "utils/joystick/joystick.h"
#include "gbk.h"

MainWindow::MainWindow(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(a2w("主窗体"));

#ifdef RUN_EMBEDDED
#include <QWSServer>
    QWSServer::setCursorVisible(false);
#else
    this->move((qApp->desktop()->width() - width()) / 2, (qApp->desktop()->height() - height()) / 2);
#endif
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_labTest_clicked()
{
    LabelTest* w = new LabelTest;
    w->show();
}

void MainWindow::on_btnTest_clicked()
{
    ButtonTest* w = new ButtonTest;
    w->show();
}

void MainWindow::on_dialogTest_clicked()
{
    DialogTest* w = new DialogTest;
    w->show();
}

void MainWindow::on_animationTest_clicked()
{
    AnimationTest* w = new AnimationTest;
    w->show();
}

void MainWindow::on_wheelTest_clicked()
{
#ifdef QT_GREATER_NEW
    bool touch = qApp->arguments().contains(QLatin1String("--touch"));
    WheelTest* w = new WheelTest(touch);
    w->show();
#else
    Toast* toast = new Toast(this, a2w("当前版本不支持"));
    toast->toast();
#endif
}

void MainWindow::on_timeSetTest_clicked()
{
#ifdef QT_GREATER_NEW
    TimeTotal* w = new TimeTotal;
    w->setWindowTitle(a2w("时间设置组件"));
    w->show();
#else
    Toast* toast = new Toast(this, a2w("当前版本不支持"));
    toast->toast();
#endif
}

void MainWindow::on_deskTopTest_clicked()
{
    Desktop* w = new Desktop;
    w->show();
}

void MainWindow::on_listTest_clicked()
{
    ListTest* w = new ListTest;
    w->show();
}

void MainWindow::on_databaseTest_clicked()
{
    DataBaseWig* w = new DataBaseWig;
    w->setWindowTitle(a2w("数据库测试"));
    w->show();
    return;
}

void MainWindow::on_progressBar2Test_clicked()
{
    ProgressBar2* w = new ProgressBar2;
    w->setWindowTitle(a2w("双路进度条"));
    w->show();
}

void MainWindow::on_digitalInputTest_clicked()
{
    DigitalInputTest* w = new DigitalInputTest;
    w->show();
}

void MainWindow::on_navBarTest_clicked()
{
    NavbarTest* w = new NavbarTest;
    w->show();
}

void MainWindow::on_titleBtn_clicked()
{
    QWidget* w = new QWidget;
    w->resize(500, 500);

    TitleBar* titleWig = new TitleBar(w);
    titleWig->show();
}

void MainWindow::on_btn_5_clicked()
{
    MyListWidget* w = new MyListWidget;
    w->setSize(200, this->frameGeometry().height());
    w->addItem(a2w("智能家居"));
    w->addItem(a2w("远程控制"));
    w->addItem(a2w("环境温度"));
    w->addItem(a2w("基础配置"));
    w->show();
}

void MainWindow::on_serialBtn_clicked()
{
    SerialDebug* serialWig = new SerialDebug;
    serialWig->show();
}

void MainWindow::on_aesTest_clicked()
{
    //  示例1
    //  实例化Aes加密类，CBC加密模式，PKCS7填充，密钥key长度16位、24位，32位分别对应QAESEncryption::AES_128、QAESEncryption::AES_192、QAESEncryption::AES_256
    QAESEncryption encryption(QAESEncryption::AES_256, QAESEncryption::CBC, QAESEncryption::PKCS7);
    QByteArray plainText("320583201709190302");
    QByteArray key("af739bab83f74db3a63245d42a14d811");
    QByteArray iv("af739bab83f74db3");
    //  Aes加密
    QByteArray encodedText = encryption.encode(plainText, key, iv);
    //  Aes解密
    QByteArray decodeText = encryption.decode(encodedText, key, iv);

    QString decodedString = QString(encryption.removePadding(decodeText));
    qDebug() << "CBC model, PKS7 padding, key length 32, iv length as 16, instantiate class method:"
             << "\n          encryption:" << encodedText.toBase64() << "\n          decryption:" << decodedString;

    //  示例2
    //  直接调用静态函数加密、解密
    QByteArray encodedText1 = QAESEncryption::Crypt(QAESEncryption::AES_256, QAESEncryption::CBC,
        "320583201709190302", "af739bab83f74db3a63245d42a14d811", "af739bab83f74db3", QAESEncryption::PKCS7);
    QByteArray decodeText1 = QAESEncryption::RemovePadding(QAESEncryption::Decrypt(
                                                               QAESEncryption::AES_256, QAESEncryption::CBC,
                                                               encodedText1, "af739bab83f74db3a63245d42a14d811", "af739bab83f74db3",
                                                               QAESEncryption::PKCS7),
        QAESEncryption::PKCS7);
    qDebug() << "CBC model, PKS7 padding, key length 32, iv length as 16, static member method:"
             << "\n          encryption:" << encodedText1.toBase64() << "\n          decryption:" << decodeText1;

    //  示例3
    //  32位key, 16位偏移量,ECB模式, 使用QString字符串
    QString inputStr("320583201709190302");
    QString key1("af739bab83f74db3a63245d42a14d811");
    QString iv1("af739bab83f74db3");

#ifdef QT_GREATER_NEW
    QByteArray hashKey = QCryptographicHash::hash(key1.toLocal8Bit(), QCryptographicHash::Sha256);
#else
    QByteArray hashKey = QCryptographicHash::hash(key1.toLocal8Bit(), QCryptographicHash::Sha1);
#endif
    QByteArray hashIV = QCryptographicHash::hash(iv1.toLocal8Bit(), QCryptographicHash::Md5);
    //  加密
    QByteArray encrypted = QAESEncryption::Crypt(QAESEncryption::AES_256, QAESEncryption::CBC,
        inputStr.toLocal8Bit(), hashKey, hashIV);
    //  解密
    QByteArray decoded = QAESEncryption::Decrypt(QAESEncryption::AES_256, QAESEncryption::CBC,
        encrypted, hashKey, hashIV,
        QAESEncryption::ISO);
    QString decodedString1 = QString(QAESEncryption::RemovePadding(decoded, QAESEncryption::ISO));

    qDebug() << "EBC model, ISO padding, key length as 32, iv length as 16, static member method:"
             << "\n          encryption:" << encrypted.toBase64() << "\n          decryption:" << decodedString1;

    //  示例4
    //  32位key, 32位偏移量， CBC模式, 使用QString字符串
    QAESEncryption encryption2(QAESEncryption::AES_256, QAESEncryption::CBC);

    QString inputStr2("320583201709190302");
    QString key2("af739bab83f74db3a63245d42a14d811");
    QString iv2("af739bab83f74db3a63245d42a14d811");

#ifdef QT_GREATER_NEW
    QByteArray hashKey2 = QCryptographicHash::hash(key1.toLocal8Bit(), QCryptographicHash::Sha256);
#else
    QByteArray hashKey2 = QCryptographicHash::hash(key1.toLocal8Bit(), QCryptographicHash::Sha1);
#endif
    QByteArray hashIV2 = QCryptographicHash::hash(iv2.toLocal8Bit(), QCryptographicHash::Md5);
    //  加密
    QByteArray encodeText2 = encryption2.encode(inputStr2.toLocal8Bit(), hashKey2, hashIV2);
    //  解密
    QByteArray decodeText2 = encryption2.decode(encodeText2, hashKey2, hashIV2);
    QString decodedString2 = QString(encryption2.removePadding(decodeText2));

    qDebug() << "CBC model, ISO padding, key length as 32,iv length as 32, instantiate class method:"
             << "\n          encryption:" << encodeText2.toBase64() << "\n          decryption:" << decodedString2;
}

void MainWindow::on_progressState_clicked()
{
    TestProgress* w = new TestProgress;
    w->show();
}

void MainWindow::on_marqueeTest_clicked()
{
    MarqueeLabelTest* w = new MarqueeLabelTest;
    w->show();
}

void MainWindow::on_buttonTest_clicked()
{
    MyButtonTest* w = new MyButtonTest;
    w->show();
}

void MainWindow::on_progressBarTest_clicked()
{
    MyProgressTest* w = new MyProgressTest;
    w->show();
}

void MainWindow::on_lineEditTest_clicked()
{
    LineEditWidgetTest* w = new LineEditWidgetTest;
    w->show();
}

void MainWindow::on_btn_13_clicked()
{
    JoyStick* w = new JoyStick;
    w->show();
}

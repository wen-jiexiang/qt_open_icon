﻿#include <QCoreApplication>
#include <QTest>
#include "qaesencryption.h"
#include <QDebug>
#include <QCryptographicHash>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //  示例1
    //  实例化Aes加密类，CBC加密模式，PKCS7填充，密钥key长度16位、24位，32位分别对应QAESEncryption::AES_128、QAESEncryption::AES_192、QAESEncryption::AES_256
    QAESEncryption encryption(QAESEncryption::AES_256, QAESEncryption::CBC,QAESEncryption::PKCS7);
    QByteArray plainText("320583201709190302");
    QByteArray key("af739bab83f74db3a63245d42a14d811");
    QByteArray iv("af739bab83f74db3");
    //  Aes加密
    QByteArray encodedText = encryption.encode(plainText, key, iv);
    //  Aes解密
    QByteArray decodeText = encryption.decode(encodedText, key, iv);

    QString decodedString = QString(encryption.removePadding(decodeText));
    qDebug()<<"encryption:"<<encodedText.toBase64()<<"\ndecryption:"<<decodedString;

    //  示例2
    //  直接调用静态函数加密、解密
    QByteArray encodedText1 = QAESEncryption::Crypt(QAESEncryption::AES_256, QAESEncryption::CBC, "320583201709190302", "af739bab83f74db3a63245d42a14d811","af739bab83f74db3",QAESEncryption::PKCS7);
    QByteArray decodeText1 = QAESEncryption::RemovePadding(QAESEncryption::Decrypt(\
                                                               QAESEncryption::AES_256, QAESEncryption::CBC, \
                                                               encodedText1, "af739bab83f74db3a63245d42a14d811","af739bab83f74db3",\
                                                               QAESEncryption::PKCS7),QAESEncryption::PKCS7);
    qDebug()<<"encryption:"<<encodedText1.toBase64()<<"\ndecryption:"<<decodeText1;


    //  示例3
    //  32位key, ECB模式, 使用QString字符串
    QString inputStr("320583201709190302");
    QString key1("af739bab83f74db3a63245d42a14d811");
    QString iv1("af739bab83f74db3");

    QByteArray hashKey = QCryptographicHash::hash(key1.toLocal8Bit(), QCryptographicHash::Sha256);
    QByteArray hashIV = QCryptographicHash::hash(iv1.toLocal8Bit(), QCryptographicHash::Md5);
    //  加密
    QByteArray encrypted = QAESEncryption::Crypt(QAESEncryption::AES_256, QAESEncryption::CBC,
                            inputStr.toLocal8Bit(), hashKey, hashIV);
    //  解密
    QByteArray decoded = QAESEncryption::Decrypt(QAESEncryption::AES_256, QAESEncryption::CBC, \
                                                 encrypted, hashKey,hashIV,\
                                                 QAESEncryption::ISO);
    QString decodedString1 = QString(QAESEncryption::RemovePadding(decoded, QAESEncryption::ISO));

    qDebug()<<"encryption:"<<encrypted.toBase64()<<"\ndecryption:"<<decodedString1;


    //  示例4
    //  32位key, CBC模式, 使用QString字符串
    QAESEncryption encryption2(QAESEncryption::AES_256, QAESEncryption::CBC);

    QString inputStr2("320583201709190302");
    QString key2("af739bab83f74db3a63245d42a14d811");
    QString iv2("af739bab83f74db3a63245d42a14d811");

    QByteArray hashKey2 = QCryptographicHash::hash(key2.toLocal8Bit(), QCryptographicHash::Sha256);
    QByteArray hashIV2 = QCryptographicHash::hash(iv2.toLocal8Bit(), QCryptographicHash::Md5);
    //  加密
    QByteArray encodeText2 = encryption2.encode(inputStr2.toLocal8Bit(), hashKey2, hashIV2);
    //  解密
    QByteArray decodeText2 = encryption2.decode(encodeText2, hashKey2, hashIV2);
    QString decodedString2 = QString(encryption2.removePadding(decodeText2));

    qDebug()<<"encryption:"<<encodeText2.toBase64()<<"\ndecryption:"<<decodedString2;

    return 0;
}



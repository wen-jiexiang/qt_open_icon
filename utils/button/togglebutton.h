/**
 ** @author:	 Greedysky
 ** @date:       2019.4.15
 ** @brief:      按钮组件
 */
#ifndef TOGGLEBUTTON_H
#define TOGGLEBUTTON_H

#include <QAbstractButton>

class QState;
class QStateMachine;
class ToggleButton;

class ToggleThumb : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(qreal shift WRITE setShift READ shift)
    Q_PROPERTY(QColor thumbColor WRITE setThumbColor READ thumbColor)
public:
    explicit ToggleThumb(ToggleButton *parent = 0);

    void setShift(qreal shift);

    inline qreal shift() const { return m_shift; }
    inline qreal offset() const { return m_offset; }

    void setThumbColor(const QColor &color);
    inline QColor thumbColor() const { return m_thumbColor; }

protected:
    virtual bool eventFilter(QObject *obj, QEvent *event) override;
    virtual void paintEvent(QPaintEvent *event) override;

    void updateOffset();

    ToggleButton *m_toggle;
    QColor m_thumbColor;
    qreal m_shift, m_offset;

};


class ToggleTrack : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QColor trackColor WRITE setTrackColor READ trackColor)
public:
    explicit ToggleTrack(ToggleButton *parent = 0);

    void setTrackColor(const QColor &color);
    inline QColor trackColor() const { return m_trackColor; }

protected:
    virtual bool eventFilter(QObject *obj, QEvent *event) override;
    virtual void paintEvent(QPaintEvent *event) override;

    ToggleButton *m_toggle;
    QColor m_trackColor;

};


class ToggleButton : public QAbstractButton
{
    Q_OBJECT
public:
    explicit ToggleButton(QWidget *parent = nullptr);
    ~ToggleButton();

    void setDisabledColor(const QColor &color);
    QColor disabledColor() const;

    void setActiveColor(const QColor &color);
    QColor activeColor() const;

    void setInactiveColor(const QColor &color);
    QColor inactiveColor() const;

    void setTrackColor(const QColor &color);
    QColor trackColor() const;

    void setOrientation(Qt::Orientation orientation);
    Qt::Orientation orientation() const;

    virtual QSize sizeHint() const override;

protected:
    virtual void paintEvent(QPaintEvent *event) override;

    void setupProperties();

    ToggleTrack *m_track;
    ToggleThumb *m_thumb;
    QStateMachine *m_stateMachine;
    QState *m_offState, *m_onState;
    Qt::Orientation m_orientation;
    QColor m_disabledColor, m_activeColor, m_inactiveColor, m_trackColor;

};

#endif

#include "lineeditwidgettest.h"
#include "ui_lineeditwidgettest.h"

LineEditWidgetTest::LineEditWidgetTest(QWidget *parent)
    : QWidget(parent),
    ui(new Ui::LineEditWidgetTest)
{
    ui->setupUi(this);

    ui->widget->setColor(QColor(0,0,0));
    ui->widget_2->setColor(QColor(0x52, 0x11, 0x88));

    ui->widget_3->setFixedSize(130, 40);
    ui->widget_3->setPlaceholderText("输入号码");
}

LineEditWidgetTest::~LineEditWidgetTest()
{
    delete ui;
}

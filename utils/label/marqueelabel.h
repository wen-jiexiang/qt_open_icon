/**
 ** @author:	 Greedysky
 ** @date:       2019.4.15
 ** @brief:      跑马灯
 */
#ifndef MARQUEELABEL_H
#define MARQUEELABEL_H

#include <QWidget>

class MarqueeLabel : public QWidget
{
    Q_OBJECT
public:
    explicit MarqueeLabel(QWidget *parent = nullptr);

    void setText(const QString &newText);
    inline QString text() const { return m_myText.trimmed(); }

    QSize sizeHint() const;

protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void showEvent(QShowEvent *event) override;
    virtual void hideEvent(QHideEvent *event) override;
    virtual void timerEvent(QTimerEvent *event) override;
    virtual void resizeEvent(QResizeEvent *event) override;

private:
    QString m_myText;
    int m_offset, m_myTimerId;

};

#endif // MARQUEELABEL_H

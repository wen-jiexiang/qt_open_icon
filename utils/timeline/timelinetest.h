#ifndef TIMELINETEST_H
#define TIMELINETEST_H

#include <QWidget>

namespace Ui {
class TimeLineTest;
}

class TimeLineTest : public QWidget
{
    Q_OBJECT

public:
    explicit TimeLineTest(QWidget *parent = nullptr);
    ~TimeLineTest();

private:
    Ui::TimeLineTest *ui;
};

#endif // TIMELINETEST_H
